import globalHelper from "./js/globalHelper.mjs"
import init1GalleryCore from "./js/1GalleryCore.mjs"
import initCardSizer from "./js/cardSizer.mjs"
import initNavigation from "./js/navigation.mjs"
import initScroll from "./js/scroll"
import initAutoPlay from "./js/autoPlay.mjs"
import initZoom from "./js/zoom.mjs"

export default function init(){
    globalHelper();
    document.querySelectorAll('._1gallery').forEach(g=>{
        if(g.classList.contains('js')) return;
        g.classList.add('js');
        init1GalleryCore(g);
        initNavigation(g);
        initCardSizer(g);
        initScroll(g);
        initAutoPlay(g);
        initZoom(g);
    });
}
docReady(init);
