export default function initScroll(galleryNode) {
    const self = galleryNode;

    self.initPrevNext = () => {
        self.resizePrevNext()
        self.querySelector('.prev').addEventListener('click', self.prev);
        self.querySelector('.next').addEventListener('click', self.next);
        window.addEventListener('resize',self.resizePrevNext);

        // mobile swipe https://css-tricks.com/simple-swipe-with-vanilla-javascript/
        function unify(e) { return e.changedTouches ? e.changedTouches[0] : e };
        let initialX = "not dragging";
        let initialSliderX;
        async function press(e){
            tryPreventingDefault(e);
            self.classList.remove('smooth');
            await sleep(0);
            initialX = unify(e).clientX;
            initialSliderX = self.slider.offsetLeft;
        }
        function drag(e){
            if("not dragging" === initialX) return;
            tryPreventingDefault(e);
            self.slider.style.left = `${Math.round(initialSliderX + unify(e).clientX - initialX)}px`
        }
        async function release(e){
            const actualX = unify(e).clientX;
            self.classList.add('smooth');
            if (actualX-initialX > window.innerWidth/20){
                self.prev(unify(e));
            } else if (initialX-actualX > window.innerWidth/20){
                self.next(unify(e));
            } else {
                self.centerActiveCard();
            }
            initialX = "not dragging";
        }
        self.slider.addEventListener('mousedown', press);
        self.slider.addEventListener('touchstart', press);
        self.slider.addEventListener('mousemove', drag);
        self.slider.addEventListener('touchmove', drag);
        self.slider.addEventListener('mouseup', release);
        self.slider.addEventListener('touchend', release);
        self.slider.addEventListener('touchcancel',release);
        self.uglySwipZoomFirefoxFix = release;
        //self.slider.addEventListener('mouseleave',release); // déplacé dans zoom.mjs en conditionnel pour éviter les conflit au dézoom via esc dans firefox. (gère le glissé hors zone de galerie).
        //self.slider.addEventListener('touchleave',release);


    }
    self.resizePrevNext = ()=>{
        const resizeButton = btn=>{
            btn.style.height = `${btn.offsetWidth}px`
            btn.style.marginTop = `-${btn.offsetWidth/2}px`
        }
        resizeButton(self.querySelector('.prev'));
        resizeButton(self.querySelector('.next'));
    }
    self.initCardCounter = () => {
        self.cardCounter = self.querySelector('.cardCounter');

        if (self.cardsCount < 10) {
            self.cardCounter.classList.add('dots')
            const dots = [];
            for (let i = 0; i < self.cardsCount; i++) dots.push(`<a class="dot" href="#card-${i + 1}"></a>`); // ⬤
            self.cardCounter.innerHTML = dots.join('');
            Array.from(self.cardCounter.children).forEach((n, i) => {
                n.addEventListener('click', (e) => self.changeActiveCard(self.cardsOrdered[i]) && tryPreventingDefault(e));
            })
        } else {
            self.cardCounter.classList.add('arrowNav')
            self.cardCounter.innerHTML = `
                <a class="first" href="#firstCard">«</a>
                <a class="prev" href="#previousCard">‹</a>
                <span class="currentCard"></span>
                /
                <span class="totalCards">${self.cardsCount}</span>
                <a class="next" href="#nextCard">›</a>
                <a class="last" href="#lastCard">»</a>
            `;
            const onDo = (selector, action) => self.cardCounter.querySelector(selector).addEventListener('click', action);
            onDo('.first', self.first);
            onDo('.prev', self.prev);
            onDo('.next', self.next);
            onDo('.last', self.last);
        }
        self.updateCardsCounter();
    }
    self.updateCardsCounter = () => {
        const activeNumber = self.cardsOrdered.indexOf(self.activeCard());
        if (self.cardsCount < 10) {
            const oldActive = self.cardCounter.querySelector('.active');
            if (oldActive) oldActive.classList.remove('active');
            self.cardCounter.children[activeNumber].classList.add('active');
        } else self.cardCounter.querySelector('.currentCard').innerHTML = `${activeNumber + 1}`;
    }

    self.initPrevNext();
    self.initCardCounter();
};
