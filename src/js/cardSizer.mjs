export default function initSizer(galleryNode) {
    const self = galleryNode;

    self.updateSize = ()=> self.cardsOrdered.forEach(card=>card.updateSize());

    self.cardsOrdered.forEach((card)=>initCard(card,self));
    self.updateSize();
    window.addEventListener('resize',()=>{
        self.centerActiveCardInstantly(); // include updateSize
    });
}
function initCard(card, galleryNode){
    const self = card;
    self.gallery = galleryNode;
    self.updateSize = ()=>{
        const cardContentNode = self.querySelector(".cardContent");
        const galleryRatio = self.gallery.offsetWidth / self.gallery.offsetHeight;
        const screenRatio = window.innerWidth / window.innerHeight;
        if(self.classList.contains('cover')){
            cardContentNode.style.width = `${self.gallery.offsetWidth}px`;
            cardContentNode.style.height = `${self.gallery.offsetHeight}px`;
        } else if(self.classList.contains('ratio')){
            const ratioWH = cardContentNode.className.match(/ratio-([0-9]+)-([0-9]+)/)
            const cardRatio = ratioWH[1]/ratioWH[2];
            if(cardRatio>galleryRatio) {
                cardContentNode.style.width = `${self.gallery.offsetWidth}px`;
                cardContentNode.style.height = `${self.gallery.offsetWidth/cardRatio}px`;
            } else {
                cardContentNode.style.width = `${self.gallery.offsetHeight*cardRatio}px`;
                cardContentNode.style.height = `${self.gallery.offsetHeight}px`;
            }
        } else { // auto
            if(screenRatio>galleryRatio) {
                cardContentNode.style.width = `${self.gallery.offsetWidth}px`;
                cardContentNode.style.height = `${self.gallery.offsetWidth/screenRatio}px`;
            } else {
                cardContentNode.style.width = `${self.gallery.offsetHeight*screenRatio}px`;
                cardContentNode.style.height = `${self.gallery.offsetHeight}px`;
            }
        }
    }
}
