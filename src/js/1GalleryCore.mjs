export default (galleryNode)=> {
    const self = galleryNode;

    self.updateCardsCount = () => self.cardsCount = self.querySelectorAll('.card').length;
    self.rearrangeCardsAroundActiveCard = () => {
        self.classList.remove('smooth');
        const afterActive = self.querySelectorAll('.card.active ~ .card').length;
        let toPush = Math.floor(self.cardsCount / 2) - afterActive;
        if (toPush > 0) {
            for (; toPush > 0; toPush--) self.push(self.shiftInPlace());
        }
        if (toPush < 0) {
            for (; toPush < 0; toPush++) self.unshiftInPlace(self.pop());
        }
        self.classList.add('smooth');
    }
    self.activeCard = () => self.querySelector('.card.active');
    self.firstCard = () => self.querySelectorAll('.card')[0];
    self.lastCard = () => self.querySelectorAll('.card')[self.cardsCount - 1];
    self.pop = function pop() {
        const lastCard = self.lastCard();
        self.slider.removeChild(lastCard);
        self.cardsCount--;
        return lastCard;
    }
    self.push = function push(card) {
        self.slider.appendChild(card);
        self.cardsCount++;
    }
    self.shift = function shift() {
        const firstCard = self.firstCard();
        self.slider.removeChild(firstCard);
        self.cardsCount--;
        return firstCard;
    }
    self.shiftInPlace = function shiftInPlace() {
        const refCard = self.firstCard();
        const cardFullWidth = refCard.nextElementSibling.offsetLeft - refCard.offsetLeft;
        self.slider.style.left = `${self.slider.offsetLeft + cardFullWidth}px`;
        const res = self.shift();
        self.slider.offsetLeft; // to fix a update issue making transition to jump.
        return res;
    }
    self.unshift = function unshift(card) {
        self.slider.insertBefore(card, self.firstCard());
        self.cardsCount++;
    }
    self.unshiftInPlace = function unshiftInPlace(card) {
        self.unshift(card);
        const refCard = self.firstCard();
        const cardFullWidth = refCard.nextElementSibling.offsetLeft - refCard.offsetLeft;
        self.slider.style.left = `${self.slider.offsetLeft - cardFullWidth}px`;
        self.slider.offsetLeft; // to fix a update issue making transition to jump.
    }

    self.first = (e) => self.changeActiveCard(self.cardsOrdered[0]) && tryPreventingDefault(e);
    self.prev = (e) => self.changeActiveCard(self.activeCard().previousElementSibling) && tryPreventingDefault(e);
    self.next = (e) => self.changeActiveCard(self.activeCard().nextElementSibling) && tryPreventingDefault(e);
    self.last = (e) => self.changeActiveCard(self.cardsOrdered[self.cardsCount - 1]) && tryPreventingDefault(e);

    self.changeActiveCard = (newActiveCard) => {
        const oldActiveCard = self.activeCard();
        oldActiveCard.classList.remove('active');
        newActiveCard.classList.add('active');
        self.rearrangeCardsAroundActiveCard();
        self.updateCardsCounter();
        self.centerActiveCard();
        return true;
    }

    self.initSlider = () => self.slider = self.querySelector('.slideArea');
    self.initCardOrder = () => self.cardsOrdered = Array.from(self.querySelectorAll('.card'));

    if (!self.activeCard()) self.firstCard().classList.add('active');
    self.updateCardsCount();
    self.initCardOrder();
    self.initSlider();
    self.rearrangeCardsAroundActiveCard();

};
