export default function init(galleryNode){
    const self = galleryNode;

    self.zoomToggle = async (e)=>{
        tryPreventingDefault(e)
        if(self.classList.contains('fullScreen')) await self.zoomOut();
        else await self.zoomFullScreen();
        return false;
    }
    self.zoomFullScreen = async ()=>{
        self.slider.removeEventListener('mouseleave',self.uglySwipZoomFirefoxFix);

        self.classList.add('fullScreen');
        try{await self.requestFullscreen();}catch (e){console.error(e)}
        self.activateKeyboardNav();
        self.resizePrevNext();
        await self.centerActiveCardInstantly();

        // to try to mitigate firefox resize/fullscreen random behavior and false size delay
        await sleep(100);
        await self.centerActiveCardInstantly();
    }
    self.zoomOut = async ()=>{
        self.classList.remove('fullScreen');
        try{await document.exitFullscreen();}catch (e){}
        self.disableKeyboardNav();
        await sleep(0);
        self.resizePrevNext();
        await self.centerActiveCardInstantly();

        // to try to mitigate firefox resize/fullscreen random behavior and false size delay
        await sleep(100);
        await self.centerActiveCardInstantly();

        self.slider.addEventListener('mouseleave',self.uglySwipZoomFirefoxFix);

    }
    self.keyUpFunc = (event)=>{
        switch (event.code){
            case "ArrowLeft" : self.prev(); break;
            case "Space" : case "ArrowRight" : self.next(); break;
            case "PageUp" : case "Home" : self.first(); break;
            case "PageDown" : case "End" : self.last(); break;
            case "Digit1" : if(self.cardsOrdered[0]) self.changeActiveCard(self.cardsOrdered[0]); else return; break;
            case "Digit2" : if(self.cardsOrdered[1]) self.changeActiveCard(self.cardsOrdered[1]); else return; break;
            case "Digit3" : if(self.cardsOrdered[2]) self.changeActiveCard(self.cardsOrdered[2]); else return; break;
            case "Digit4" : if(self.cardsOrdered[3]) self.changeActiveCard(self.cardsOrdered[3]); else return; break;
            case "Digit5" : if(self.cardsOrdered[4]) self.changeActiveCard(self.cardsOrdered[4]); else return; break;
            case "Digit6" : if(self.cardsOrdered[5]) self.changeActiveCard(self.cardsOrdered[5]); else return; break;
            case "Digit7" : if(self.cardsOrdered[6]) self.changeActiveCard(self.cardsOrdered[6]); else return; break;
            case "Digit8" : if(self.cardsOrdered[7]) self.changeActiveCard(self.cardsOrdered[7]); else return; break;
            case "Digit9" : if(self.cardsOrdered[8]) self.changeActiveCard(self.cardsOrdered[8]); else return; break;
            case "Digit0" : if(self.cardsOrdered[9]) self.changeActiveCard(self.cardsOrdered[9]); else return; break;
            default:
                return;
        }
        tryPreventingDefault(event)
    }
    self.activateKeyboardNav = ()=>{
        document.addEventListener('keyup', self.keyUpFunc);
    }
    self.disableKeyboardNav = ()=>{
        document.removeEventListener('keyup',self.keyUpFunc);
    }

    if(!self.classList.contains('zoomable')) return;
    const zoomButton = document.createElement('a');
    zoomButton.classList.add('zoom');
    zoomButton.setAttribute('href','#fullScreen');
    zoomButton.innerHTML = `<span>🔍</span>`;
    zoomButton.addEventListener('click', self.zoomToggle);
    self.appendChild(zoomButton);

    document.addEventListener('fullscreenchange', async ()=> {
        if (!document.fullscreenElement && self.classList.contains('fullScreen')) await self.zoomOut()
    });
};
