export default (galleryNode) => {
    const self = galleryNode;

    self.initAutoPlay = ()=>{
        const isAutoPlay = self.className.match(/autoplay-([0-9]+)s/);
        if(!isAutoPlay) return;
        const playSpeed = parseInt(isAutoPlay[1]);
        setInterval(self.autoNext,playSpeed*1000);

        const setPauseArea = (pauseArea)=>{
            pauseArea.addEventListener('mouseenter', ()=>self.pause=true);
            pauseArea.addEventListener('mouseleave', ()=>self.pause=false);
        }
        setPauseArea(self.cardCounter);
        setPauseArea(self.querySelector('.prev'));
        setPauseArea(self.querySelector('.next'));

        // Don't auto-play when dragging / swapping
        self.slider.addEventListener('mousedown', ()=>self.pause=true);
        self.slider.addEventListener('touchstart', ()=>self.pause=true);
        self.slider.addEventListener('mouseup', ()=>self.pause=false);
        self.slider.addEventListener('touchend', ()=>self.pause=false);
        self.slider.addEventListener('touchcancel',()=>self.pause=false);

    }
    self.autoNext = ()=>{
        if(self.pause) return;
        self.next();
    }
    self.initAutoPlay();
};
