export default async (galleryNode) => {
    const self = galleryNode;

    self.centerActiveCard = () => {
        if(typeof self.updateSize !== "undefined") self.updateSize();
        const galleryWidth = self.offsetWidth;
        const activeCardWidth = self.activeCard().offsetWidth;
        const activeCardLeft = self.activeCard().offsetLeft;
        self.slider.style.left = `${galleryWidth / 2 - activeCardLeft - activeCardWidth / 2}px`;
    }
    self.centerActiveCardInstantly = async () => {
        self.classList.remove('smooth');
        await sleep(0);
        self.classList.remove('smooth');
        self.centerActiveCard();
        await sleep(0);
        self.classList.add('smooth');
    }

    await self.centerActiveCardInstantly();
};
