# 1gallery
## [Online Demo](https://1000i100.frama.io/1gallery/)

## Usage :
For examples, see [demo source code](demo/index.html.ejs).

Require [node.js](https://nodejs.org/).

In your project root, in cli :
```shell
npm install --save-dev 1gallery ejs
```
In your [ejs](https://www.npmjs.com/package/ejs) parsed files when you want insert a 1gallery :
```ejs
<%
const galleryOptions = 'banner zoomable autoplay-5s'
const exampleCards = [];
exampleCards.push(`<section class="card cover"><h1><center>A full size card/slide</center></h1></section>`);
exampleCards.push(`<img class="card" src="img/1forma-tic-fond-ecran.png"/>`);
%>
<%- include('../PATH_TO/node_modules/1gallery/1gallery.ejs',{cards:exampleCards,classes:galleryOptions}); %>

<link rel="stylesheet" href="../PATH_TO/node_modules/1gallery/1gallery.css"/>
<script src="../PATH_TO/node_modules/1gallery/1gallery.js"></script>
```

You can use css class to customize 1gallery behavior on **gallery scope** or **card scope**.

## Gallery class :

- **zoomable** : add fullscreen mode and related icon.
- **autoplay-[1-999]s** : automatically go to next slide every [0-999] seconds

- **banner** : banner style gallery (height : 150px)
- **splash** : splash style gallery (height : 100vh)

PS : you can use css to override any default 1gallery css.

## Card class :
- **card** : **required** to let the gallery engine detect each card/slide
- **auto** : **default** resize the card to match screen ratio.
- **cover** : extend the card to the gallery size like css `object-fit: cover;`
- **ratio-[1-9999]-[1-9999]** : force card ratio to [1-9999]/[1-9999]. Usefull for photo.

PS : you can use *ratio* and *cover* on the same card for some strange html slide use-case.
