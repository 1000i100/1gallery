# 1gallery

## Usage :

Chaque élément de galerie est une `.card`.

Pour chaque galerie, importez le modèle :
```
<%- include('node_modules/1gallery/gallery.ejs',{htmlCards:'htmlCards',classes:'splash'}); %>
ou
<%- include('node_modules/1gallery/galleryFromPath.ejs',{path:'exempleGallery/',classes:'banner'}); %>
```
en remplaçant le contenu html par vos cards (qui peuvent être générées par 1gallery)
et en incluant les classes que vous voulez pour personnaliser le style, que ce soit parmi les thèmes par défaut ou avec vos propres personnalisations.

Pour chaque page incluant 1gallery incluez également le css et le js.


